package;


import flash.display.Sprite;
import flash.geom.Matrix3D;
import flash.geom.Vector3D;
import flash.geom.Rectangle;
import flash.events.Event;
import openfl.display.OpenGLView;
import openfl.gl.GL;
import openfl.gl.GLBuffer;
import openfl.gl.GLTexture;
import openfl.gl.GLFramebuffer;
import openfl.gl.GLRenderbuffer;
import openfl.gl.GLProgram;
import openfl.utils.Float32Array;
import haxe.Timer;

import nme.Lib;

class Main extends Sprite {
	
	
	private var shaderProgram:GLProgram;
	private var vertexAttribute:Int;
	private var vertexBuffer:GLBuffer;
	private var colorAttribute:Int;
	private var colorBuffer:GLBuffer;



	//Texture ID
	private var textureID:GLTexture; 

	//Buffers
	private var depthRenderBuffer:GLRenderbuffer;
	private var frameBuffer:GLFramebuffer;
	
	#if ios
		private var currentFrameBuffer:GLFramebuffer;
	#end


	//View
	private var view:OpenGLView;
	
	//Rotate angle
	private var rotateY:Float;

	private var _stage:Dynamic;




	public function new () {
		
		super ();
		
		if (OpenGLView.isSupported) {
			
			rotateY = 0.0;

			//Create OpenGL view. There are special module to render OpenGL.
			view = new OpenGLView ();
			
			//Load shaders.
			createProgram ();
			
			var vertices = [
			 -1.0, -1.0,  1.0,
			  1.0, -1.0,  1.0,
			 -1.0,  1.0,  1.0,
			  1.0,  1.0,  1.0,
			 -1.0, -1.0, -1.0,
			  1.0, -1.0, -1.0,
			 -1.0,  1.0, -1.0,
			  1.0,  1.0, -1.0				
			];
		


			var color_vertices = [
			  1.0,  0.0,  0.0, 1.0,
			  0.0,  1.0,  0.0, 1.0,
			  0.0,  0.0,  1.0, 1.0,
			  1.0,  1.0,  0.0, 1.0,
			  0.0,  1.0,  1.0, 1.0,
			  1.0,  1.0,  0.0, 1.0,
			  1.0,  1.0,  1.0, 1.0,
			  0.0,  0.0,  1.0, 1.0,				
			];

			 _stage = Lib.current.stage;
			
			//Load model to draw.
			vertexBuffer = GL.createBuffer();
			GL.bindBuffer (GL.ARRAY_BUFFER, vertexBuffer);	
			GL.bufferData (GL.ARRAY_BUFFER, new Float32Array (cast vertices), 
						   GL.STATIC_DRAW);
			GL.enableVertexAttribArray (vertexAttribute);
			GL.bindBuffer (GL.ARRAY_BUFFER, vertexBuffer);
			GL.vertexAttribPointer (vertexAttribute, 3, GL.FLOAT, false, 0, 0);
			GL.bindBuffer (GL.ARRAY_BUFFER, null);


			//Set vertex buffer and color buffer.
			colorBuffer = GL.createBuffer();
			GL.bindBuffer (GL.ARRAY_BUFFER, colorBuffer);
			GL.bufferData (GL.ARRAY_BUFFER, new Float32Array (cast color_vertices), 
				           GL.STATIC_DRAW );
			GL.enableVertexAttribArray (colorAttribute);
			GL.bindBuffer (GL.ARRAY_BUFFER, colorBuffer);
			GL.vertexAttribPointer (colorAttribute, 4, GL.FLOAT, false, 0, 0);
			GL.bindBuffer (	GL.ARRAY_BUFFER, null);


			//Get current framebuffer handle.
			#if ios
			trace ("IOS: Get current framebuffer's handle.");
			currentFrameBuffer 
				= new GLFramebuffer( GL.version, 
									 GL.getParameter( 
									 	GL.FRAMEBUFFER_BINDING) 
									 );
			trace ("IOS: Finished getting current framebuffer's handle.");

			#end

			trace ("Setup FBO.");

			//Setup FBO.
			setupFBO(256, 256);		

			//Render view
			view.render = renderView; //Set render() as render function.
			addChild (view);

			//Depth configuration
			GL.enable( GL.DEPTH_TEST );
			GL.depthFunc( GL.LESS );


	        _stage.addEventListener(
                nme.events.Event.ENTER_FRAME,
	                function(): Void{
						rotateY = rotateY + 10.0;
					} );
			//Setup timer
			/*
			var glTimer:haxe.Timer = new haxe.Timer( 100 );
			glTimer.run = function(): Void{
				rotateY = rotateY + 10.0;
			};
			*/


		}
			
	}
	
	
	private function createProgram ():Void {
		
		var vertexShaderSource = 
			"
			attribute vec3 vertexPosition;
			attribute vec4 vertexColor;
			varying vec4 vColor;

			uniform mat4 modelViewMatrix;
			uniform mat4 projectionMatrix;
			
			void main(void) {
				gl_Position = projectionMatrix * modelViewMatrix * vec4(vertexPosition, 1.0);
				vColor = vertexColor;
			}";

		var vertexShader = GL.createShader (GL.VERTEX_SHADER);
		GL.shaderSource (vertexShader, vertexShaderSource);
		GL.compileShader (vertexShader);
		
		if (GL.getShaderParameter (vertexShader, GL.COMPILE_STATUS) == 0) {
			
			throw "Error compiling vertex shader";
			
		}
		
		var fragmentShaderSource = 
			
			"
			varying vec4 vColor;

			void main(void) {
				gl_FragColor = vColor;
			}";
		
		var fragmentShader = GL.createShader (GL.FRAGMENT_SHADER);
		
		GL.shaderSource (fragmentShader, fragmentShaderSource);
		GL.compileShader (fragmentShader);
		
		if (GL.getShaderParameter (fragmentShader, GL.COMPILE_STATUS) == 0) {
			
			throw "Error compiling fragment shader";
			
		}
		
		shaderProgram = GL.createProgram ();
		GL.attachShader (shaderProgram, vertexShader);
		GL.attachShader (shaderProgram, fragmentShader);
		GL.linkProgram (shaderProgram);
		
		if (GL.getProgramParameter (shaderProgram, GL.LINK_STATUS) == 0) {
			
			throw "Unable to initialize the shader program.";
			
		}
		
		vertexAttribute = GL.getAttribLocation (shaderProgram, "vertexPosition");
		colorAttribute  = GL.getAttribLocation (shaderProgram, "vertexColor");


	}


	public function setupFBO( width:Int, height:Int ){

		trace( 'framebuffer = (' + width, height + ')');

		frameBuffer = GL.createFramebuffer();
		depthRenderBuffer = GL.createRenderbuffer();

	
		//Create and attach texture to framebuffer.
		textureID = GL.createTexture();
		GL.bindTexture(GL.TEXTURE_2D, textureID);
		GL.texImage2D( GL.TEXTURE_2D , 
						0 , 
						GL.RGB, 
						Std.int (width) , Std.int (height), 
						0, 
						GL.RGB, 
						GL.UNSIGNED_SHORT_5_6_5, 
						null );
		GL.texParameteri( GL.TEXTURE_2D , 
						  GL.TEXTURE_WRAP_S,
						  GL.CLAMP_TO_EDGE );
		GL.texParameteri( GL.TEXTURE_2D , 
						  GL.TEXTURE_WRAP_T,
						  GL.CLAMP_TO_EDGE );		
		GL.texParameteri( GL.TEXTURE_2D , 
						  GL.TEXTURE_MIN_FILTER ,
						  GL.LINEAR );
		GL.texParameteri( GL.TEXTURE_2D , 
						  GL.TEXTURE_MAG_FILTER,
						  GL.LINEAR );


		//Bind renderbuffer and create depth buffer. 
		GL.bindRenderbuffer( GL.RENDERBUFFER, depthRenderBuffer );
		GL.renderbufferStorage( GL.RENDERBUFFER, GL.DEPTH_COMPONENT16,
								width , height);

		//Bind the framebuffer
		GL.bindFramebuffer( GL.FRAMEBUFFER, frameBuffer );

		//Specify texture as color attachement.
		GL.framebufferTexture2D( GL.FRAMEBUFFER, 
						 GL.COLOR_ATTACHMENT0, 
						 GL.TEXTURE_2D, 
						 textureID, 
						 0);

		//Specify depthRenderbuffer as depth attachement.
		GL.framebufferRenderbuffer( GL.FRAMEBUFFER , 
									GL.DEPTH_ATTACHMENT,
									GL.RENDERBUFFER,
									depthRenderBuffer);


		//Check framebuffer.
		var status = GL.checkFramebufferStatus( GL.FRAMEBUFFER );
		switch( status ){

			case GL.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				trace("FRAMEBUFFER_INCOMPLETE_ATTACHMENT");

			case GL.FRAMEBUFFER_UNSUPPORTED:
				trace("GL_FRAMEBUFFER_UNSUPPORTED");

			case GL.FRAMEBUFFER_COMPLETE:
				trace("GL_FRAMEBUFFER_COMPLETE");

			default:
				trace("Check frame buffer:", status);
		}

	}

	
	private function renderView (rect:Rectangle):Void {

			

		//Projection matrix
		var projectionMatrix = Matrix3D.createOrtho (-rect.width/4,  rect.width/4, 
													 -rect.height/4, rect.height/4, 
													 200, -200);
		//Model view matrix
		var modelViewMatrix = new Matrix3D();
		modelViewMatrix.prependTranslation( 0, 0, 0.0);
		modelViewMatrix.prependRotation( rotateY, new Vector3D( 0.0, 1.0, 0.0) );
		modelViewMatrix.prependRotation( 30.0, new Vector3D( 0.0, 0.0, 1.0) );
		modelViewMatrix.prependScale( 30.0, 30.0, 30.0);


		//Set projection and modelview matrix and apply them into shader.
		var projectionMatrixUniform = GL.getUniformLocation (shaderProgram, "projectionMatrix");
		var modelViewMatrixUniform = GL.getUniformLocation (shaderProgram, "modelViewMatrix");
		GL.uniformMatrix3D (projectionMatrixUniform, false, projectionMatrix);
		GL.uniformMatrix3D (modelViewMatrixUniform, false, modelViewMatrix);



		//Clear current screen with red color and draw object on the framebuffer linked to screen. 
		//=========================================
		GL.viewport (Std.int (rect.x), Std.int (rect.y), 
				     Std.int (rect.width), Std.int (rect.height));

		GL.clearColor (1.0, 0.0, 0.0, 1.0);
		GL.clear (GL.DEPTH_BUFFER_BIT |GL.COLOR_BUFFER_BIT);

		//Load shader.
		GL.useProgram (shaderProgram);

		//Draw cubes.
		GL.drawArrays (GL.TRIANGLE_STRIP, 0, 8);
		//==========================================

		
		//Render on texture linked to framebuffer. 
		//========================================== 
		GL.bindFramebuffer( GL.FRAMEBUFFER, frameBuffer );
		trace( 'framebuffer on texture =', frameBuffer);

		GL.viewport( 0, 0, 256, 256); 
		GL.clearColor (0.0, 0.0, 1.0, 1.0);
		GL.clear (GL.DEPTH_BUFFER_BIT | GL.COLOR_BUFFER_BIT);

		//Load shader.
		GL.useProgram (shaderProgram);

		//Draw cubes.
		GL.drawArrays (GL.TRIANGLE_STRIP, 0, 8);

		//Release framebuffer linked with texture. 
		#if ios
			GL.bindFramebuffer( GL.FRAMEBUFFER, currentFrameBuffer );
			trace( 'current window framebuffer =', currentFrameBuffer);
		#else
			GL.bindFramebuffer( GL.FRAMEBUFFER, null );
			//trace( 'current window framebuffer.');
		#end 
		//==========================================


		//Check gl error. 
		if( GL.getError() == GL.INVALID_FRAMEBUFFER_OPERATION ){
			trace("INVALID_FRAMEBUFFER_OPERATION!!");
		}

		
	}
	
	
}